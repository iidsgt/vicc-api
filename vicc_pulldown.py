#!/usr/bin/env python
import argparse
import csv
from functools import reduce
import logging
#import maf
import pandas
import requests
import sys
from concurrent.futures import ThreadPoolExecutor as PoolExecutor
from urllib.parse import quote

logging.basicConfig(level=logging.INFO)

def parse_args():
    parser = argparse.ArgumentParser(description="Welcome to VICCTOR!")
    parser.add_argument("--vicc_url", help="Base url for VICC api.")
    return parser.parse_args()


def gen_es_query():
    logging.info("Building query for elastic search.")
    query_symbols = ["*"]
    return query_symbols

def make_request_to_vicc(url, query):
    total = 0
    logging.info("Making calls to VICC.")
    for q in query:
        logging.debug(f"Querying: {q}")
        try:
            r = requests.get(f"{url}/api/v1/associations?size=1&q={q}")
        except Exception as e:
            logging.info(e)
            return 
        logging.debug(r.json())
        total += int(r.json()['hits']['total'])

    logging.info(f"Total Records in VICC: {total}")


    def query_vicc(query, offset):
        logging.debug(f"Calling: {offset}")
        try:
            r = requests.get(f"{url}/api/v1/associations?size={10}&offset={offset}&q={query}")
        except Exception as e:
            logging.info(e)
            return 
        return r.json()

    biomarkers = set()

    with PoolExecutor(max_workers=4) as executor:
        q = query * len(list(range(1, total, 10)))
       # print(len(q))
        for r in executor.map(query_vicc, q, range(1, total, 10)):
            for x in r['hits']['hits']:
                for y in x['features']:
                    #print(y['biomarker_type'])
                    biomarkers.add(y['biomarker_type'])
    with open('biomarkers.txt', 'w') as fp:
        fp.write("\n".join(biomarkers))

    # for i in range(1, total, 10):
    #     logging.info(f"Calling: {i}")
    #     try:
    #         r = requests.get(f"{url}/api/v1/associations?size={10}&offset={i}&q={query}")
    #     except Exception as e:
    #         logging.info(e)
    #         return 

        #associations += r.json()['hits']['hits']

def main():

    print("Welcome to VICCTOR!")

    args = parse_args()
    merged_df = make_request_to_vicc(args.vicc_url,
        gen_es_query()
    )

    logging.info("Merging DataFrames.")
    #Add filtering for funcotator output.

    logging.info("Writing Dataframe to CSV.")


if __name__ == "__main__":
    main()

###  Funcotator 4.1.1.0 | Date 20201126T021133 | Gencode 27 CANONICAL | ACMGLMMLof 1 | ACMG_recommendation SF_v2.0 | ClinVar_VCF 20180429_hg38 | LMMKnown 2018061
###  Funcotator 4.1.1.0 | Date 20200917T120921 | Gencode 28 CANONICAL | Achilles 110303 | CGC full_2012_03-15 | Cosmic v84 | CosmicFusion v84 | CosmicTissue v83 | DNARepairGenes 20180524T145835  | Familial_Cancer_Genes 20110905 | Gencode_XHGNC 90_38 | Gencode_XRefSeq 90_38 | HGNC Nov302017 | Oreganno 20160119 | Simple_Uniprot 2014_12 | chr1_a_bed 2018-11-20T16:34:38 | chr1_b_bed 2018-11-20T16:34:41 | dbSNP 9606_b150

#Funcotator  --output annotated_variants --ref-version hg38 --data-sources-path /keep/46ccd4b3c350ad48938c6a1890724946+19344/funcotator_dataSources.v1.6.20190124s --output-file-format MAF --variant /keep/73ea55219932d79d60de5cb46bd77dec+65/vcf_counted.vcf --reference /keep/5bd8dd5a733df65a31e57a41e0ae51e6+174737/references/gencode38/bwa/GRCh38.primary_assembly.genome.fa  --remove-filtered-variants false --transcript-selection-mode CANONICAL --five-prime-flank-size 5000 --three-prime-flank-size 0 --lookahead-cache-bp 100000 --force-b37-to-hg19-reference-contig-conversion false --interval-set-rule UNION --interval-padding 0 --interval-exclusion-padding 0 --interval-merging-rule ALL --read-validation-stringency SILENT --seconds-between-progress-updates 10.0 --disable-sequence-dictionary-validation false --create-output-bam-index true --create-output-bam-md5 false --create-output-variant-index true --create-output-variant-md5 false --lenient false --add-output-sam-program-record true --add-output-vcf-command-line true --cloud-prefetch-buffer 40 --cloud-index-prefetch-buffer -1 --disable-bam-index-caching false --sites-only-vcf-output false --help false --version false --showHidden false --verbosity INFO --QUIET false --use-jdk-deflater false --use-jdk-inflater false --gcs-max-retries 20 --gcs-project-for-requester-pays  --disable-tool-default-read-filters false
#Funcotator  --output annotated_variants --ref-version hg38 --data-sources-path /keep/b1b9da4ee0b61f14ca8f250d73f030e1+5274/funcotator_dataSources.v1.6.20190124_Germline --output-file-format MAF --variant /keep/aa7f044a09c323b0de24b3d1c34eedb9+110/ApplyVQSR.vcf.gz --reference /keep/5bd8dd5a733df65a31e57a41e0ae51e6+174737/references/gencode38/bwa/GRCh38.primary_assembly.genome.fa  --remove-filtered-variants false --transcript-selection-mode CANONICAL --five-prime-flank-size 5000 --three-prime-flank-size 0 --lookahead-cache-bp 100000 --force-b37-to-hg19-reference-contig-conversion false --interval-set-rule UNION --interval-padding 0 --interval-exclusion-padding 0 --interval-merging-rule ALL --read-validation-stringency SILENT --seconds-between-progress-updates 10.0 --disable-sequence-dictionary-validation false --create-output-bam-index true --create-output-bam-md5 false --create-output-variant-index true --create-output-variant-md5 false --lenient false --add-output-sam-program-record true --add-output-vcf-command-line true --cloud-prefetch-buffer 40 --cloud-index-prefetch-buffer -1 --disable-bam-index-caching false --sites-only-vcf-output false --help false --version false --showHidden false --verbosity INFO --QUIET false --use-jdk-deflater false --use-jdk-inflater false --gcs-max-retries 20 --gcs-project-for-requester-pays  --disable-tool-default-read-filters false