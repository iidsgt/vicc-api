FROM continuumio/miniconda3

# Construct the conda environment
COPY environment.yml .
RUN conda update -n base -c defaults conda && \
    conda env update --name base --file /environment.yml && \
	rm /environment.yml

COPY sonofvicctor.py /bin/

RUN chmod +x /bin/sonofvicctor.py

CMD ["python","/bin/sonofvicctor.py"]