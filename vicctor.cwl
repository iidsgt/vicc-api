cwlVersion: v1.0
class: CommandLineTool

requirements:
  DockerRequirement:
    dockerPull: "sinaiiidgst/vicc-api:c529d01c2f174698eca03c556e173e7c63e91fd5"
  InlineJavascriptRequirement: {}

inputs:
  InputMAF:
    type: File
    inputBinding:
      position: 100

  Url:
    type: string
    default: "https://search.cancervariants.org"
    inputBinding:
      prefix: "--vicc_url"

  Output:
    type: string
    default: "vicc_maf.csv"
    inputBinding:
      prefix: "--output"
      valueFrom: "vicc_maf.csv"

baseCommand: ["vicc.py"]

outputs:
  vicc_maf:
    type: File
    outputBinding:
      glob: "vicc_maf.csv"