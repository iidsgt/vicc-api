import argparse
from elasticsearch import Elasticsearch
import pandas
import numpy
import json

es = Elasticsearch(['http://elastic:changeme@localhost'])

def load_json(fp):
    try:
        es.indices.delete(index='vicc')
    except:
        pass
    body='{"settings": { "index.mapping.coerce": true, "index.mapping.total_fields.limit": 3000}, "mappings": {"properties":{"brca.Date_last_evaluated_ENIGMA": {"type": "text"} } } } '
    es.indices.create('vicc', body=body)
    #data = pandas.DataFrame([])
    for i, line in enumerate(fp.readlines()):
        data = json.loads(line)
        es.index(index='vicc',
                 body=data,
                 id=i,
                 refresh=True)
        print(f"Inserted Line {i}")
        
def load_civic(fp):
    try:
        es.indices.delete(index='civic')
    except:
        pass
    body='{"settings": { "index.mapping.coerce": true, "index.mapping.total_fields.limit": 3000 } }'
    es.indices.create('civic', body=body)
    data = pandas.read_csv(fp, sep="\t")
    data = data.replace(numpy.nan, "")
    for i, line in enumerate(data.to_dict(orient='records')):
        es.index(index='civic',
                 body=line,
                 id=i,
                 refresh=True)
        print(f"Loading Civic {i}")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file")
    parser.add_argument("--type", required=True)
    args = parser.parse_args()

    if args.type == "vicc":
        with open(args.file) as fp:
            load_json(fp)
    if args.type == "civic":
        with open(args.file) as fp:
            load_civic(fp)

if __name__ == "__main__":
    main()