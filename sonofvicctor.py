#!/usr/bin/env python
import argparse
import csv
from functools import reduce
import logging
import elasticsearch
import pandas
import requests
import sys
from concurrent.futures import ThreadPoolExecutor as PoolExecutor
from urllib.parse import quote
from elasticsearch_dsl import Search



logging.basicConfig(level=logging.INFO)

def parse_args():
    parser = argparse.ArgumentParser(description="Welcome to VICCTOR!")
    parser.add_argument("MAF", help="MAF file from funcotator or other source.")
    parser.add_argument("--es_url", help="Base url for ES with credentials", required=True)
    parser.add_argument("--type", help="Type of file being inputted.", required=True)
    parser.add_argument("--sample_id", help="Sample lid of patient.", required=True)
    parser.add_argument("--output", help="Output File", required=True)
    return parser.parse_args()


def maf_to_datframe(fn):
    logging.info("Loading MAF to Dataframe.")
    maf = []
    with open(fn) as f:
        for line in f.readlines():
            if "#" in line[0]:
                continue
            else:
                line_list = line.rstrip().split('\t')
                maf.append(line_list)
    headers = maf[0]
    body = maf[1:]
    named_tuples = list(zip(headers, *body))
    df = pandas.DataFrame.from_records(named_tuples).transpose()
    df.columns = df.iloc[0]
    df = df[1:]
    df = df.iloc[:,:-1]
    logging.debug(df.columns)
    df = df[[
        "Hugo_Symbol",
        "Entrez_Gene_Id",
        "Chromosome",
        "Start_Position",
        "End_Position",
        "Strand",
        "Variant_Classification",
        "Variant_Type",
        "Reference_Allele",
        "Tumor_Seq_Allele1",
        "Tumor_Seq_Allele2",
        "dbSNP_RS",
        "dbSNP_Val_Status",
        "Genome_Change",
        "Annotation_Transcript",
        "cDNA_Change",
        "Codon_Change",
        "Protein_Change",
        "Refseq_mRNA_Id",
        "Refseq_prot_Id",
        "SwissProt_acc_Id",
        "Description",
        "COSMIC_overlapping_mutations",
        "COSMIC_fusion_genes",
        "COSMIC_tissue_types_affected",
        "DrugBank",
        "tumor_f",
        "t_alt_count",
        "t_ref_count",
        "n_alt_count",
        "n_ref_count",
        "HGNC_Ensembl_Gene_ID"
    ]]
    df = df[df['HGNC_Ensembl_Gene_ID'] != '']
    return df

def filter_mutation_types(df, types=None):
    logging.info(f"Filtering for mutations types: {types}")
    if isinstance(types, dict):
        return df[~df['Variant_Classification'].isin(types)]
    if isinstance(types, str):
        return df[df['Variant_Classification'] != types]

def filter_maf_fields(df, fields=None):
    return df.drop(df.columns.difference(fields), 1)


def make_calls_to_vicc(df, url_string, fp):

    def rsetattr(obj, attr, val):
        pre, _, post = attr.rpartition('.')
        return setattr(rgetattr(obj, pre) if pre else obj, post, val)

    # using wonder's beautiful simplification: https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects/31174427?noredirect=1#comment86638618_31174427

    def rgetattr(obj, attr, *args):
        def _getattr(obj, attr):
            return getattr(obj, attr, *args)
        return reduce(_getattr, [obj] + attr.split('.'))

    def try_or_null(source, value):
        try:
            return rgetattr(source, value)
        except:
            return "null"
    results = []
    host = url_string.split("@")[1]
    user = url_string.split(":")[0]
    passw = url_string.split("@")[0].split(":")[1]
    es = elasticsearch.Elasticsearch(hosts=[host], http_auth=(user,passw))
    for r in df.to_dict(orient="records"):
        s = Search(using=es, index="vicc")
        res = s.query("multi_match", query=r["Protein_Change"], fields=["*"])
        res = res.query("match", gene_identifiers__ensembl_gene_id=r["HGNC_Ensembl_Gene_ID"])
        for resy in res.scan():
            results.append({
                "ENSID": r["HGNC_Ensembl_Gene_ID"],
                "Protein_Change": r["Protein_Change"],
                "genes": "|".join(resy.genes),
                "source": resy.source or "null",
                "association.description": try_or_null(resy, "association.description"),
                "association.drug_labels": try_or_null(resy, "association.drug_labels"),
                "association.evidence_label": try_or_null(resy, "association.evidence_label") ,
                "association.evidence_level": try_or_null(resy, "association.evidence_label"),
                "association.phenotype.description": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.family": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.type.term": try_or_null(resy, "association.phenotype.type.term"),
                "association.response_type": try_or_null(resy, "association.response_type") ,
                "association.variant_name": try_or_null(resy, "association.variant_name"),
                "association.feature_names": try_or_null(resy, "feature_names"),
                "association.variant_name": try_or_null(resy, "variant_name"),
            })
    ld = pandas.DataFrame(results)
    return ld

def make_calls_to_civic(df, url_string, fp):

    def rsetattr(obj, attr, val):
        pre, _, post = attr.rpartition('.')
        return setattr(rgetattr(obj, pre) if pre else obj, post, val)

    # using wonder's beautiful simplification: https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects/31174427?noredirect=1#comment86638618_31174427

    def rgetattr(obj, attr, *args):
        def _getattr(obj, attr):
            return getattr(obj, attr, *args)
        return reduce(_getattr, [obj] + attr.split('.'))

    def try_or_null(source, value):
        try:
            return rgetattr(source, value)
        except:
            return "null"
    results = []
    host = url_string.split("@")[1]
    user = url_string.split(":")[0]
    passw = url_string.split("@")[0].split(":")[1]
    es = elasticsearch.Elasticsearch(hosts=[host], http_auth=(user,passw))
    for r in df.to_dict(orient="records"):
        s = Search(using=es, index="civic")
        res = s.query("multi_match", query=r["Protein_Change"], fields=["variant"])
        for resy in res.scan():
            results.append({
                "ENSID": r["HGNC_Ensembl_Gene_ID"],
                "Protein_Change": r["Protein_Change"],
                "genes": resy.gene,
                "clinical_significance": resy.clinical_significance,
                "drugs": try_or_null(resy, "drugs"),
                "evidence_status": try_or_null(resy, "evidence_status") ,
                "evidence_type": try_or_null(resy, "evidence_type"),
                "phenotypes": try_or_null(resy, "phenotypes"),
                "rating": try_or_null(resy, "rating"),
                "variant": try_or_null(resy, "variant"),
                "variant_origin": try_or_null(resy, "variant_origin") ,
            })
    ld = pandas.DataFrame(results)
    return ld

def query_fusions(fp, url_string):
    def rsetattr(obj, attr, val):
        pre, _, post = attr.rpartition('.')
        return setattr(rgetattr(obj, pre) if pre else obj, post, val)

    # using wonder's beautiful simplification: https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects/31174427?noredirect=1#comment86638618_31174427

    def rgetattr(obj, attr, *args):
        def _getattr(obj, attr):
            return getattr(obj, attr, *args)
        return reduce(_getattr, [obj] + attr.split('.'))

    def try_or_null(source, value):
        try:
            return rgetattr(source, value)
        except:
            return "null"


    maf = []
    with open(fp) as f:
        for line in f.readlines():
            if "#" in line[0]:
                line = line[1:]
                maf.append(line.rstrip().split("\t"))
            else:
                line_list = line.rstrip().split('\t')
                maf.append(line_list)

    headers = maf[0]
    body = maf[1:]
    named_tuples = list(zip(headers, *body))
    df = pandas.DataFrame.from_records(named_tuples).transpose()
    df.columns = df.iloc[0]
    df = df[1:]
    df = df.iloc[:,:-1]

    host = url_string.split("@")[1]
    user = url_string.split(":")[0]
    passw = url_string.split("@")[0].split(":")[1]
    es = elasticsearch.Elasticsearch(hosts=[host], http_auth=(user,passw))
    results = []
    for r in df.to_dict(orient="records"):
        s = Search(using=es, index="vicc")
        res = s.query("multi_match", query=r["gene1"], fields=["*"])
        #res = res.query("multi_match",  query="fusion", fields=["*"])
        for resy in res.scan():
            print(resy)
            results.append({
                "GENE": r["gene1"],
                "genes": "|".join(resy.genes),
                "source": resy.source or "null",
                "association.description": try_or_null(resy, "association.description"),
                "association.drug_labels": try_or_null(resy, "association.drug_labels"),
                "association.evidence_label": try_or_null(resy, "association.evidence_label") ,
                "association.evidence_level": try_or_null(resy, "association.evidence_label"),
                "association.phenotype.description": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.family": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.type.term": try_or_null(resy, "association.phenotype.type.term"),
                "association.response_type": try_or_null(resy, "association.response_type") ,
                "association.variant_name": try_or_null(resy, "association.variant_name"),
                "association.feature_names": try_or_null(resy, "feature_names"),
                "association.variant_name": try_or_null(resy, "variant_name"),
            })

        s = Search(using=es, index="vicc")
        res = s.query("multi_match", query=r["gene2"], fields=["*"])
        #res = res.query("multi_match",  query="fusion", fields=["*"])
        for resy in res.scan():
            print(resy)
            results.append({
                "GENE": r["gene2"],
                "genes": "|".join(resy.genes),
                "source": resy.source or "null",
                "association.description": try_or_null(resy, "association.description"),
                "association.drug_labels": try_or_null(resy, "association.drug_labels"),
                "association.evidence_label": try_or_null(resy, "association.evidence_label") ,
                "association.evidence_level": try_or_null(resy, "association.evidence_label"),
                "association.phenotype.description": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.family": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.type.term": try_or_null(resy, "association.phenotype.type.term"),
                "association.response_type": try_or_null(resy, "association.response_type") ,
                "association.variant_name": try_or_null(resy, "association.variant_name"),
                "association.feature_names": try_or_null(resy, "feature_names"),
                "association.variant_name": try_or_null(resy, "variant_name"),
            })

        s = Search(using=es, index="civic")
        res = s.query("multi_match", query=r["gene1"], fields=["*"])
        #res = res.query("multi_match",  query="fusion", fields=["*"])
        for resy in res.scan():
            results.append({
                "GENE": r["gene1"],
                "genes": resy.gene,
                "clinical_significance": resy.clinical_significance,
                "drugs": try_or_null(resy, "drugs"),
                "evidence_status": try_or_null(resy, "evidence_status") ,
                "evidence_type": try_or_null(resy, "evidence_type"),
                "phenotypes": try_or_null(resy, "phenotypes"),
                "rating": try_or_null(resy, "rating"),
                "variant": try_or_null(resy, "variant"),
                "variant_origin": try_or_null(resy, "variant_origin") ,
            })

        s = Search(using=es, index="civic")
        res = s.query("multi_match", query=r["gene2"], fields=["*"])
        #res = res.query("multi_match",  query="fusion", fields=["*"])
        for resy in res.scan():
            results.append({
                "GENE": r["gene1"],
                "genes": resy.gene,
                "clinical_significance": resy.clinical_significance,
                "drugs": try_or_null(resy, "drugs"),
                "evidence_status": try_or_null(resy, "evidence_status") ,
                "evidence_type": try_or_null(resy, "evidence_type"),
                "phenotypes": try_or_null(resy, "phenotypes"),
                "rating": try_or_null(resy, "rating"),
                "variant": try_or_null(resy, "variant"),
                "variant_origin": try_or_null(resy, "variant_origin") ,
            })

        return pandas.DataFrame(results)


def query_cnvs(fp, url_string):
    def rsetattr(obj, attr, val):
        pre, _, post = attr.rpartition('.')
        return setattr(rgetattr(obj, pre) if pre else obj, post, val)

    # using wonder's beautiful simplification: https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects/31174427?noredirect=1#comment86638618_31174427

    def rgetattr(obj, attr, *args):
        def _getattr(obj, attr):
            return getattr(obj, attr, *args)
        return reduce(_getattr, [obj] + attr.split('.'))

    def try_or_null(source, value):
        try:
            return rgetattr(source, value)
        except:
            return "null"

    df = pandas.read_csv(fp, delimiter="\t")
    host = url_string.split("@")[1]
    user = url_string.split(":")[0]
    passw = url_string.split("@")[0].split(":")[1]
    es = elasticsearch.Elasticsearch(hosts=[host], http_auth=(user, passw))
    results = []
    # print(df.shape)
    for r in df.to_dict(orient="records"):
        s = Search(using=es, index="vicc")
        res = s.query("multi_match", query=r["geneID"], fields=["*"])
        # print(r["geneID"])
        for resy in res.scan():
            # print(resy)
            results.append({
                "GENE": r["geneID"],
                "genes": "|".join(resy.genes),
                "source": resy.source or "null",
                "association.description": try_or_null(resy, "association.description"),
                "association.drug_labels": try_or_null(resy, "association.drug_labels"),
                "association.evidence_label": try_or_null(resy, "association.evidence_label") ,
                "association.evidence_level": try_or_null(resy, "association.evidence_label"),
                "association.phenotype.description": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.family": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.type.term": try_or_null(resy, "association.phenotype.type.term"),
                "association.response_type": try_or_null(resy, "association.response_type") ,
                "association.variant_name": try_or_null(resy, "association.variant_name"),
                "association.feature_names": try_or_null(resy, "feature_names"),
                "association.variant_name": try_or_null(resy, "variant_name"),
            })

        s = Search(using=es, index="civic")
        res = s.query("multi_match", query=r["geneID"], fields=["*"])
        #res = res.query("multi_match",  query="fusion", fields=["*"])
        for resy in res.scan():
            results.append({
                "GENE": r["geneID"],
                "genes": resy.gene,
                "clinical_significance": resy.clinical_significance,
                "drugs": try_or_null(resy, "drugs"),
                "evidence_status": try_or_null(resy, "evidence_status") ,
                "evidence_type": try_or_null(resy, "evidence_type"),
                "phenotypes": try_or_null(resy, "phenotypes"),
                "rating": try_or_null(resy, "rating"),
                "variant": try_or_null(resy, "variant"),
                "variant_origin": try_or_null(resy, "variant_origin") ,
            })
        # print(pandas.DataFrame(results))
    return pandas.DataFrame(results)

def query_gene_expression(fp, url_string, sample_id):
    def rsetattr(obj, attr, val):
        pre, _, post = attr.rpartition('.')
        return setattr(rgetattr(obj, pre) if pre else obj, post, val)

    # using wonder's beautiful simplification: https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects/31174427?noredirect=1#comment86638618_31174427

    def rgetattr(obj, attr, *args):
        def _getattr(obj, attr):
            return getattr(obj, attr, *args)
        return reduce(_getattr, [obj] + attr.split('.'))

    def try_or_null(source, value):
        try:
            return rgetattr(source, value)
        except:
            return "null"

    df = pandas.read_csv(fp, delimiter="\t")

    df = df[['Unnamed: 0', sample_id]]

    df_up = df[df[sample_id] > 2]
    df_down = df[df[sample_id] < -2]

    ### Query for upregulated genes
    host = url_string.split("@")[1]
    user = url_string.split(":")[0]
    passw = url_string.split("@")[0].split(":")[1]
    es = elasticsearch.Elasticsearch(hosts=[host], http_auth=(user, passw))
    results = []

    for r in df_up.to_dict(orient="records"):
        s = Search(using=es, index="vicc")
        res = s.query("multi_match", query=r['Unnamed: 0'], fields=["*"])
        res = res.query("multi_match", query="upregulated", fields=["*"])
        for resy in res.scan():
            results.append({
                "GENE": r['Unnamed: 0'],
                "genes": "|".join(resy.genes),
                "source": try_or_null(resy, "source"),
                "association.description": try_or_null(resy, "association.description"),
                "association.drug_labels": try_or_null(resy, "association.drug_labels"),
                "association.evidence_label": try_or_null(resy, "association.evidence_label"),
                "association.evidence_level": try_or_null(resy, "association.evidence_label"),
                "association.phenotype.description": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.family": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.type.term": try_or_null(resy, "association.phenotype.type.term"),
                "association.response_type": try_or_null(resy, "association.response_type") ,
                "association.variant_name": try_or_null(resy, "association.variant_name"),
                "association.feature_names": try_or_null(resy, "feature_names"),
                "association.variant_name": try_or_null(resy, "variant_name"),
            })

        s = Search(using=es, index="civic")
        res = s.query("multi_match", query=r['Unnamed: 0'], fields=["*"])
        res = res.query("multi_match", query="upregulated", fields=["*"])
        #res = res.query("multi_match",  query="fusion", fields=["*"])
        for resy in res.scan():
            results.append({
                "GENE": r['Unnamed: 0'],
                "genes": resy.gene,
                "clinical_significance": resy.clinical_significance,
                "drugs": try_or_null(resy, "drugs"),
                "evidence_status": try_or_null(resy, "evidence_status") ,
                "evidence_type": try_or_null(resy, "evidence_type"),
                "phenotypes": try_or_null(resy, "phenotypes"),
                "rating": try_or_null(resy, "rating"),
                "variant": try_or_null(resy, "variant"),
                "variant_origin": try_or_null(resy, "variant_origin") ,
            })

        # print(pandas.DataFrame(results))
    for r in df_down.to_dict(orient="records"):
        s = Search(using=es, index="vicc")
        res = s.query("multi_match", query=r['Unnamed: 0'], fields=["*"])
        res = res.query("multi_match", query="downregulated", fields=["*"])
        # print(r["geneID"])
        for resy in res.scan():
            # print(resy)
            results.append({
                "GENE": r['Unnamed: 0'],
                "genes": "|".join(resy.genes),
                "source": resy.source or "null",
                "association.description": try_or_null(resy, "association.description"),
                "association.drug_labels": try_or_null(resy, "association.drug_labels"),
                "association.evidence_label": try_or_null(resy, "association.evidence_label"),
                "association.evidence_level": try_or_null(resy, "association.evidence_label"),
                "association.phenotype.description": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.family": try_or_null(resy, "association.phenotype.family"),
                "association.phenotype.type.term": try_or_null(resy, "association.phenotype.type.term"),
                "association.response_type": try_or_null(resy, "association.response_type"),
                "association.variant_name": try_or_null(resy, "association.variant_name"),
                "association.feature_names": try_or_null(resy, "feature_names"),
                "association.variant_name": try_or_null(resy, "variant_name"),
            })

        s = Search(using=es, index="civic")
        res = s.query("multi_match", query=r['Unnamed: 0'], fields=["*"])
        res = res.query("multi_match", query="downregulated", fields=["*"])
        # res = res.query("multi_match",  query="fusion", fields=["*"])
        for resy in res.scan():
            results.append({
                "GENE": r['Unnamed: 0'],
                "genes": resy.gene,
                "clinical_significance": resy.clinical_significance,
                "drugs": try_or_null(resy, "drugs"),
                "evidence_status": try_or_null(resy, "evidence_status"),
                "evidence_type": try_or_null(resy, "evidence_type"),
                "phenotypes": try_or_null(resy, "phenotypes"),
                "rating": try_or_null(resy, "rating"),
                "variant": try_or_null(resy, "variant"),
                "variant_origin": try_or_null(resy, "variant_origin"),
            })

        # print(pandas.DataFrame(results))
    return pandas.DataFrame(results)

def main():
    print("Welcome to Son-of Vicctor")

    args = parse_args()
    if args.type == 'fusions':
        df = query_fusions(args.MAF, args.es_url)
        df.to_csv(args.output, index=False, quoting=csv.QUOTE_NONNUMERIC)
        sys.exit(0)

    if args.type == 'cnv':
        df = query_cnvs(args.MAF, args.es_url)
        df.to_csv(args.output, index=False, quoting=csv.QUOTE_NONNUMERIC)
        sys.exit(0)

    if args.type == 'expression':
        df = query_gene_expression(args.MAF, args.es_url, args.sample_id)
        df.to_csv(args.output, index=False, quoting=csv.QUOTE_NONNUMERIC)
        sys.exit(0)

    df = maf_to_datframe(args.MAF)
    df = filter_mutation_types(df, types="Intron")
    df1 = make_calls_to_vicc(df, args.es_url, args.output)
    df2 = make_calls_to_civic(df, args.es_url, args.output)
    try:
        df_a = df2.merge(df1, on="ENSID")
    except:
        if df1.shape[0] == 0 and df2.shape[0] != 0:
            df_a = df2
        if df1.shape[0] != 0 and df2.shape[0] == 0:
            df_a = df1
    df_a = pandas.merge(df, df_a, left_on="HGNC_Ensembl_Gene_ID", right_on="ENSID", left_index=False)
    df_a.to_csv(args.output, index=False, quoting=csv.QUOTE_NONNUMERIC)
    return 0
if __name__ == "__main__":
    main()